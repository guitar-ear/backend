from flask import current_app
from flask_jwt_extended import JWTManager, jwt_required
from flask_jwt_extended.view_decorators import _decode_jwt_from_headers
import jwt as jat
import json
import hashlib
import os

from api.redis import redis_client
from api.res.response import Template4XX


def _extract_jwt_rid():
    try:
        encoded_token, _ = _decode_jwt_from_headers()
    except Exception:
        return None
    unverified_claims = jat.decode(
        encoded_token, verify=False, algorithms=current_app.config['JWT_ALGORITHM']
    )
    return unverified_claims["identity"]


class RedisSession:
    secret = hashlib.sha512(os.urandom(128)).hexdigest()
    session = None
    scope = []
    jwt_secret = secret

    @staticmethod
    def get_session():
        rid = _extract_jwt_rid()
        if not rid:
            return
        try:
            RedisSession.session = json.loads(redis_client.get(rid).decode())
            RedisSession.scope = RedisSession.session["scope"]
            RedisSession.jwt_secret = RedisSession.session["jwt-secret"]
        except Exception:
            RedisSession.session = None
            RedisSession.scope = []
            RedisSession.jwt_secret = RedisSession.secret

    @staticmethod
    def get_jwt_secret() -> str:
        RedisSession.get_session()
        return RedisSession.jwt_secret


jwt = JWTManager()


@jwt.decode_key_loader
def key_decode(claims, headers):
    return RedisSession.get_jwt_secret()


@jwt.encode_key_loader
def key_encode(identity):
    return RedisSession.jwt_secret


def jwt_required_scope(scope: str):
    def decorator(func):

        @jwt_required
        def wrapper(*args, **kwargs):
            if not RedisSession.session:
                return Template4XX("AUTH_ERROR", "The credential are not valid", 401).json()
            if scope in RedisSession.scope:
                return func(*args, **kwargs)
            return Template4XX("AUTH_ERROR", "Access to this element is forbidden", 403).json()

        return wrapper

    return decorator
