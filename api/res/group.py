import werkzeug
import io
from PIL import Image
from flask import send_file
from flask_restful import Resource, reqparse

from api.jwt import jwt_required_scope
from api.res.response import Template2XX, Template4XX
from api.mod.group import Group
from api.mod.artist import Artist
from api.mod.album import Album
from api.mod.song import Song
from api.res.template import SearchItemTemplate


class Groups(Resource):
    """
    This is class is used to get all the groups

    Methods:
    -------
    get(self)
        Give back a list of groups

    Error Code:
    ------------
    GROUP_EMPTY_LIST
        No group found

    """

    @jwt_required_scope('metadata_get_groups')
    def get(self):
        groups = [group.json() for group in Group.get_all()]
        if not groups:
            return Template4XX("GROUP_EMPTY_LIST", "No group found", 404).json()
        return Template2XX("Success", 200, {"groups": groups}).json()


class GroupSearch(SearchItemTemplate):
    """
        This is class is used to search a group with a criteria

        Methods:
        -------
        post(self)
            Give back a list of groups

        Error Code:
        ------------
        GROUP_EMPTY_LIST
            No groups found

        GROUP_MALFORMED_REQUEST
            Invalid argument value

    """
    EMPTY_LIST = ("GROUP_EMPTY_LIST", "No groups found")
    MALFORMED_REQUEST = ("GROUP_MALFORMED_REQUEST", "Invalid argument value")

    return_name = "groups"
    function_search = Group
    api_endpoint = "metadata_search_group"


class GroupResource(Resource):
    """
        This is class is used to create and update a group

        Methods:
        -------
        post(self)
            Create a new group

        put(self)
            Update an existing group

        Error Code:
        ------------
        GROUP_DATA_DUPLICATION
            The given group already exist

        GROUP_ID_NOT_FOUND
            The selected group was not found

    """
    post_parser = reqparse.RequestParser()
    post_parser.add_argument(
        'name',
        type=str,
        required=True,
        help="This field cannot be blank."
    )

    put_parser = post_parser.copy()
    put_parser.add_argument(
        'id',
        type=int,
        required=True,
        help="This field cannot be blank."
    )

    @jwt_required_scope('metadata_add_group')
    def post(self):
        args = self.post_parser.parse_args()

        if Group.find_by_name(args["name"]):
            return Template4XX("GROUP_DATA_DUPLICATION", "The given name already exist", 409).json()

        group = Group(args["name"])
        group.save_to_db()
        return Template2XX("Created group", 200, group.json()).json()

    @jwt_required_scope('metadata_update_group')
    def put(self):
        args = self.put_parser.parse_args()

        group = Group.find_by_id(args["id"])
        if not group:
            return Template4XX("GROUP_DATA_DUPLICATION", "The selected group was not found", 404).json()

        group_duplicate = Group.find_by_name(args["name"])
        if group_duplicate and group.id != group_duplicate.id:
            return Template4XX("GROUP_DATA_DUPLICATION", "Another group with the same name already exist", 409).json()

        group.name = args["name"]
        group.save_to_db()
        return Template2XX("Title updated", 200, group.json()).json()


class GroupDelete(Resource):
    """
        This is class is used to delete a group

        Methods:
        -------
        delete(self, _id)
            Delete a group

        Error Code:
        ------------
        GROUP_ID_NOT_FOUND
            The selected group was not found

        """

    @jwt_required_scope('metadata_del_group')
    def delete(self, group_id: int):
        group = Group.find_by_id(group_id)
        if not group:
            return Template4XX("GROUP_ID_NOT_FOUND", "The selected group was not found", 404).json()
        group_json = group.json()
        group.delete_from_db()
        return Template2XX("Album deleted", 200, group_json).json()


class GroupArtist(Resource):
    """
        This is class is used to insert and delete artist in the group

        Methods:
        -------
        post(self)
            Add a group member

        put(self)
            Remove a group member

        Error Code:
        ------------

        GROUP_ID_NOT_FOUND
            The selected group was not found

        GROUP_MEMBER_ID_NOT_FOUND
            The selected album was not found

    """

    @jwt_required_scope('metadata_add_group_artist')
    def post(self, group_id, artist_id):
        group = Group.find_by_id(group_id)
        if not group:
            return Template4XX("GROUP_ID_NOT_FOUND", "The selected group was not found", 404).json()
        if not Artist.find_by_id(artist_id):
            return Template4XX("GROUP_MEMBER_ID_NOT_FOUND", "The selected artist was not found", 404).json()
        group.insert_artist([artist_id])
        return Template2XX("Artist added", 200, group.json()).json()

    @jwt_required_scope('metadata_del_group_artist')
    def delete(self, group_id, artist_id):
        group = Group.find_by_id(group_id)
        if not group:
            return Template4XX("GROUP_ID_NOT_FOUND", "The selected group was not found", 404).json()
        if not Artist.find_by_id(artist_id):
            return Template4XX("GROUP_MEMBER_ID_NOT_FOUND", "The selected artist was not found", 404).json()
        group.delete_artist([artist_id])
        return Template2XX("Artist removed", 200, group.json()).json()


class GroupAlbum(Resource):
    """
        This is class is used to insert and delete album from the group

        Methods:
        -------
        post(self)
            Add a album

        put(self)
            Remove a album

        Error Code:
        ------------

        GROUP_ID_NOT_FOUND
            The selected group was not found

        GROUP_ALBUM_ID_NOT_FOUND
            The selected album was not found

    """

    @jwt_required_scope('metadata_add_group_album')
    def post(self, group_id, album_id):
        group = Group.find_by_id(group_id)
        if not group:
            return Template4XX("GROUP_ID_NOT_FOUND", "The selected group was not found", 404).json()
        if not Album.find_by_id(album_id):
            return Template4XX("GROUP_ALBUM_ID_NOT_FOUND", "The selected album was not found", 404).json()
        group.insert_album([album_id])
        return Template2XX("Album added", 200, group.json()).json()

    @jwt_required_scope('metadata_del_group_album')
    def delete(self, group_id, album_id):
        group = Group.find_by_id(group_id)
        if not group:
            return Template4XX("GROUP_ID_NOT_FOUND", "The selected group was not found", 404).json()
        if not Album.find_by_id(album_id):
            return Template4XX("GROUP_ALBUM_ID_NOT_FOUND", "The selected album was not found", 404).json()
        group.delete_album([album_id])
        return Template2XX("Album removed", 200, group.json()).json()


class GroupSong(Resource):
    """
        This is class is used to insert and delete song from the group

        Methods:
        -------
        post(self)
            Add a song

        put(self)
            Remove a song

        Error Code:
        ------------

        GROUP_ID_NOT_FOUND
            The selected group was not found

        GROUP_SONG_ID_NOT_FOUND
            The selected song was not found

    """

    @jwt_required_scope('metadata_add_group_song')
    def post(self, group_id, song_id):
        group = Group.find_by_id(group_id)
        if not group:
            return Template4XX("GROUP_ID_NOT_FOUND", "The selected group was not found", 404).json()
        if not Song.find_by_id(song_id):
            return Template4XX("GROUP_SONG_ID_NOT_FOUND", "The selected song was not found", 404).json()
        group.insert_song([song_id])
        return Template2XX("Song added", 200, group.json()).json()

    @jwt_required_scope('metadata_del_group_song')
    def delete(self, group_id, song_id):
        group = Group.find_by_id(group_id)
        if not group:
            return Template4XX("GROUP_ID_NOT_FOUND", "The selected group was not found", 404).json()
        if not Song.find_by_id(song_id):
            return Template4XX("GROUP_SONG_ID_NOT_FOUND", "The selected song was not found", 404).json()
        group.delete_song([song_id])
        return Template2XX("Song removed", 200, group.json()).json()


class GroupImage(Resource):
    """
        This is class is used to inset and retrieve the group image

        Methods:
        -------
        get(self, group_id)
            Retrieve image

        post(self, group_id)
            Add a image

        Error Code:
        ------------

        GROUP_ID_NOT_FOUND
            The selected group was not found

        GROUP_IMAGE_ERROR
            The given image is not valid

        GROUP_MAX_IMAGE_SIZE
            The selected image is over 1 MB

        GROUP_IMAGE_NOT_FOUND
            The selected image was not found

    """
    image_parser = reqparse.RequestParser()
    image_parser.add_argument(
        'image',
        type=werkzeug.datastructures.FileStorage,
        required=True,
        location='files'
    )

    @jwt_required_scope('metadata_get_group_image')
    def get(self, group_id):
        group = Group.find_by_id(group_id)
        if not group:
            return Template4XX("GROUP_ID_NOT_FOUND", "The selected group was not found", 404).json()
        return send_file(io.BytesIO(group.image),
                         attachment_filename='logo.jpeg',
                         mimetype='image/jpg'
                         )

    # curl -v -X POST -H "Content-Type: multipart/form-data"
    # -F "image=@name.png" http://127.0.0.1:5000/api/v0/group/7/image-upload
    @jwt_required_scope('metadata_add_group_image')
    def post(self, group_id):
        group = Group.find_by_id(group_id)
        if not group:
            return Template4XX("GROUP_ID_NOT_FOUND", "The selected group was not found", 404).json()
        args = self.image_parser.parse_args()
        if len(args["image"].read()) > 1048576:  # Size 1 MB
            return Template4XX("GROUP_MAX_IMAGE_SIZE", "The selected image is over 1 MB", 400)
        try:
            im = Image.open(args["image"])
        except Exception:
            return Template4XX("GROUP_IMAGE_ERROR", "The given image is not valid", 400).json()
        if im.format != "PNG" and im.format != "JPEG":
            return Template4XX("GROUP_IMAGE_ERROR", "The given image is not valid", 400).json()
        if im.format == "PNG":
            im = im.convert('RGB')
        img_byte_arr = io.BytesIO()
        im.save(img_byte_arr, format='JPEG')
        group.image = img_byte_arr.getvalue()
        group.save_to_db()
        return Template2XX("Image Saved", 200).json()
