import werkzeug
import io
from PIL import Image
from flask import send_file
from flask_restful import Resource, reqparse

from api.jwt import jwt_required_scope
from api.res.response import Template2XX, Template4XX, Template4XXPayload
from api.mod.album import Album
from api.res.template import SearchItemTemplate


class Albums(Resource):
    """
    This is class is used to get all the albums

    Methods:
    -------
    get(self)
        Give back a list of albums

    Error Code:
    ------------
    ALBUM_EMPTY_LIST
        No album found

    """

    @jwt_required_scope('metadata_get_albums')
    def get(self):
        albums = [album.json() for album in Album.get_all()]
        if not albums:
            return Template4XX("ALBUM_EMPTY_LIST", "No album found", 404).json()
        return Template2XX("Success", 200, {"albums": albums}).json()


class AlbumSearch(SearchItemTemplate):
    """
        This is class is used to search a album with a criteria

        Methods:
        -------
        post(self)
            Give back a list of albums

        Error Code:
        ------------
        ALBUM_EMPTY_LIST
            No album found

        ALBUM_MALFORMED_REQUEST
            Invalid argument value

    """
    EMPTY_LIST = ("ALBUM_EMPTY_LIST", "No album found")
    MALFORMED_REQUEST = ("ALBUM_MALFORMED_REQUEST", "Invalid argument value")

    return_name = "albums"
    function_search = Album
    api_endpoint = "metadata_search_album"


class AlbumResource(Resource):
    """
        This is class is used to create and update a album

        Methods:
        -------
        post(self)
            Create a new album

        put(self)
            Update an existing album

        Error Code:
        ------------
        ALBUM_DATA_DUPLICATION
            The given album already exist

        ALBUM_ID_NOT_FOUND
            The selected album was not found

        ALBUM_SONG_ID_NOT_FOUND
            One or more song id where not found

    """
    post_parser = reqparse.RequestParser()
    post_parser.add_argument(
        'title',
        type=str,
        required=True,
        help="This field cannot be blank."
    )
    post_parser.add_argument(
        'songList',
        type=list,
        location='json',
        required=True,
        help="This field cannot be blank."
    )

    put_parser = post_parser.copy()
    put_parser.remove_argument('songList')
    put_parser.add_argument(
        'id',
        type=int,
        required=True,
        help="This field cannot be blank."
    )

    @jwt_required_scope('metadata_add_album')
    def post(self):
        args = self.post_parser.parse_args()

        if Album.find_by_title(args["title"]):
            return Template4XX("ALBUM_DATA_DUPLICATION", "The given name already exist", 409).json()

        album = Album(args["title"], args["songList"])
        response, error_list = album.save_with_transaction()
        if response:
            return Template2XX("Created album", 200, album.json()).json()
        return Template4XXPayload("ALBUM_SONG_ID_NOT_FOUND",
                                  "One or more song id where not found",
                                  404,
                                  {"songs": error_list}
                                  ).json()

    @jwt_required_scope('metadata_update_album')
    def put(self):
        args = self.put_parser.parse_args()

        album = Album.find_by_id(args["id"])
        if not album:
            return Template4XX("ALBUM_ID_NOT_FOUND", "The selected album was not found", 404).json()

        album_duplicate = Album.find_by_title(args["title"])
        if album_duplicate and album.id != album_duplicate.id:
            return Template4XX("ALBUM_DATA_DUPLICATION", "Another album with the same name already exist", 409).json()

        album.title = args["title"]
        album.save_to_db()
        return Template2XX("Title updated", 200, album.json()).json()


class AlbumDelete(Resource):
    """
        This is class is used to delete a album

        Methods:
        -------
        delete(self, _id)
            Delete a album

        Error Code:
        ------------
        ALBUM_ID_NOT_FOUND
            The selected album was not found

        """

    @jwt_required_scope('metadata_del_album')
    def delete(self, album_id: int):
        album = Album.find_by_id(album_id)
        if not album:
            return Template4XX("ALBUM_ID_NOT_FOUND", "The selected album was not found", 404).json()
        album_json = album.json()
        album.delete_from_db()
        return Template2XX("Album deleted", 200, album_json).json()


class AlbumImage(Resource):
    """
        This is class is used to inset and retrieve the album image

        Methods:
        -------
        get(self, group_id)
            Retrieve image

        post(self, group_id)
            Add a image

        Error Code:
        ------------

        ALBUM_ID_NOT_FOUND
            The selected album was not found

        ALBUM_IMAGE_ERROR
            The given image is not valid

        ALBUM_MAX_IMAGE_SIZE
            The selected image is over 1 MB

        ALBUM_IMAGE_NOT_FOUND
            The selected image was not found

    """
    image_parser = reqparse.RequestParser()
    image_parser.add_argument(
        'image',
        type=werkzeug.datastructures.FileStorage,
        required=True,
        location='files'
    )

    @jwt_required_scope('metadata_get_album_image')
    def get(self, album_id):
        album = Album.find_by_id(album_id)
        if not album:
            return Template4XX("ALBUM_ID_NOT_FOUND", "The selected album was not found", 404).json()
        return send_file(io.BytesIO(album.image),
                         attachment_filename='album.jpeg',
                         mimetype='image/jpg'
                         )

    @jwt_required_scope('metadata_add_album_image')
    def post(self, album_id):
        album = Album.find_by_id(album_id)
        if not album:
            return Template4XX("ALBUM_ID_NOT_FOUND", "The selected album was not found", 404).json()
        args = self.image_parser.parse_args()
        if len(args["image"].read()) > 1048576:  # Size 1 MB
            return Template4XX("ALBUM_MAX_IMAGE_SIZE", "The selected image is over 1 MB", 400)
        try:
            im = Image.open(args["image"])
        except Exception:
            return Template4XX("ALBUM_IMAGE_ERROR", "The given image is not valid", 400).json()
        if im.format != "PNG" and im.format != "JPEG":
            return Template4XX("ALBUM_IMAGE_ERROR", "The given image is not valid", 400).json()
        if im.format == "PNG":
            im = im.convert('RGB')
        img_byte_arr = io.BytesIO()
        im.save(img_byte_arr, format='JPEG')
        album.image = img_byte_arr.getvalue()
        album.save_to_db()
        return Template2XX("Image Saved", 200).json()
