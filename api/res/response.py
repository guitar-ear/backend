class Template2XX:
    def __init__(self, message: str, status: int, data: dict = None):
        if data is None:
            data = {}
        self.message = message
        self.status = status
        self.data = data

    def json(self) -> (dict, int):
        return \
            {
                "success": True,
                "status": self.status,
                "message": self.message,
                "data": self.data
            }, self.status


class Template4XX:
    def __init__(self, error: str, message: str, status: int):
        self.message = message
        self.error = error
        self.status = status

    def json(self) -> (dict, int):
        return \
            {
                "success": False,
                "status": self.status,
                "error": self.error,
                "message": self.message,
            }, self.status


class Template4XXPayload(Template4XX):
    def __init__(self, error: str, message: str, status: int, payload: dict):
        super().__init__(error, message, status)
        self.payload = payload

    def json(self) -> (dict, int):
        return \
            {
                "success": False,
                "status": self.status,
                "error": self.error,
                "message": self.message,
                "data": self.payload
            }, self.status
