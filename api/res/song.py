from flask_restful import Resource, reqparse

from api.jwt import jwt_required_scope
from api.res.response import Template2XX, Template4XX
from api.mod.song import Song
from api.res.template import SearchItemTemplate


class Songs(Resource):
    """
    This is class is used to get all the songs

    Methods:
    -------
    get(self)
        Give back a list of songs

    Error Code:
    ------------
    SONG_EMPTY_LIST
        No song found

    """

    @jwt_required_scope('metadata_get_songs')
    def get(self):
        songs = [song.json() for song in Song.get_all()]
        if not songs:
            return Template4XX("SONG_EMPTY_LIST", "No song found", 404).json()
        return Template2XX("Success", 200, {"songs": songs}).json()


class SongSearch(SearchItemTemplate):
    """
        This is class is used to search song with a criteria

        Methods:
        -------
        post(self)
            Give back a list of songs

        Error Code:
        ------------
        SONG_EMPTY_LIST
            No songs found

        SONG_MALFORMED_REQUEST
            Invalid argument value

    """
    EMPTY_LIST = ("SONG_EMPTY_LIST", "No songs found")
    MALFORMED_REQUEST = ("SONG_MALFORMED_REQUEST", "Invalid argument value")

    return_name = "songs"
    function_search = Song
    api_endpoint = "metadata_search_song"


class SongResource(Resource):
    """
        This is class is used to create and update a song

        Methods:
        -------
        post(self)
            Create a new song

        put(self)
            Update an existing song

        Error Code:
        ------------
        SONG_DATA_DUPLICATION
            The given title already exist

        SONG_ID_NOT_FOUND
            The selected song was not found

    """
    post_parser = reqparse.RequestParser()
    post_parser.add_argument(
        'title',
        type=str,
        required=True,
        help="This field cannot be blank."
    )

    put_parser = post_parser.copy()
    put_parser.add_argument(
        'id',
        type=int,
        required=True,
        help="This field cannot be blank."
    )

    @jwt_required_scope('metadata_add_song')
    def post(self):
        title = self.post_parser.parse_args()

        if Song.find_by_title(title["title"]):
            return Template4XX("SONG_DATA_DUPLICATION", "The given name already exist", 409).json()

        song = Song(title["title"])
        song.save_to_db()
        return Template2XX("Song created", 200, song.json()).json()

    @jwt_required_scope('metadata_update_song')
    def put(self):
        args = self.put_parser.parse_args()

        song = Song.find_by_id(args["id"])
        if not song:
            return Template4XX("SONG_ID_NOT_FOUND", "The selected song was not found", 404).json()

        song_duplicate = Song.find_by_title(args["title"])
        if song_duplicate and song.id != song_duplicate.id:
            return Template4XX("SONG_DATA_DUPLICATION", "Another song with the same name already exist", 409).json()

        song.title = args["title"]
        song.save_to_db()
        return Template2XX("Name updated", 200, song.json()).json()


class SongDelete(Resource):
    """
        This is class is used to delete a song

        Methods:
        -------
        delete(self, _id)
            Delete a song

        Error Code:
        ------------
        SONG_ID_NOT_FOUND
            The selected song was not found

        """

    @jwt_required_scope('metadata_del_song')
    def delete(self, song_id: int):
        song = Song.find_by_id(song_id)
        if not song:
            return Template4XX("SONG_ID_NOT_FOUND", "The selected song was not found", 404).json()
        song_json = song.json()
        song.delete_from_db()
        return Template2XX("Family deleted", 200, song_json).json()
