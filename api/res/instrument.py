from flask_restful import Resource, reqparse

from api.jwt import jwt_required_scope
from api.res.response import Template2XX, Template4XX
from api.mod.instrument import Family, Instrument

from api.res.template import SearchItemTemplate


class Families(Resource):
    """
    This is class is used to get all the instrument families

    Methods:
    -------
    get(self)
        Give back a list of instrument families

    Error Code:
    ------------
    FAMILY_EMPTY_LIST
        No families instrument found

    """

    @jwt_required_scope('metadata_get_families')
    def get(self):
        families = [fam.json() for fam in Family.get_all()]
        if not families:
            return Template4XX("FAMILY_EMPTY_LIST", "No families instrument found", 404).json()
        return Template2XX("Success", 200, {"families": families}).json()


class FamilySearch(SearchItemTemplate):
    """
        This is class is used to search family with a criteria

        Methods:
        -------
        post(self)
            Give back a list of families

        Error Code:
        ------------
        FAMILY_EMPTY_LIST
            No families found

        FAMILY_MALFORMED_REQUEST
            Invalid argument value

    """
    EMPTY_LIST = ("FAMILY_EMPTY_LIST", "No families found")
    MALFORMED_REQUEST = ("FAMILY_MALFORMED_REQUEST", "Invalid argument value")

    return_name = "families"
    function_search = Family
    api_endpoint = "metadata_search_family"


class FamilyResource(Resource):
    """
        This is class is used to create and update family of instrument

        Methods:
        -------
        post(self)
            Create a new family

        put(self)
            Update an existing family

        Error Code:
        ------------
        FAMILY_DATA_DUPLICATION
            The given name already exist

        FAMILY_ID_NOT_FOUND
            The selected family was not found

    """
    post_parser = reqparse.RequestParser()
    post_parser.add_argument(
        'name',
        type=str,
        required=True,
        help="This field cannot be blank."
    )

    put_parser = post_parser.copy()
    put_parser.add_argument(
        'id',
        type=int,
        required=True,
        help="This field cannot be blank."
    )

    @jwt_required_scope('metadata_add_family')
    def post(self):
        name = self.post_parser.parse_args()

        if Family.find_by_name(name["name"]):
            return Template4XX("FAMILY_DATA_DUPLICATION", "The given name already exist", 409).json()

        family = Family(name["name"])
        family.save_to_db()
        return Template2XX("Family created", 200, family.json()).json()

    @jwt_required_scope('metadata_update_family')
    def put(self):
        args = self.put_parser.parse_args()

        family = Family.find_by_id(args["id"])
        if not family:
            return Template4XX("FAMILY_ID_NOT_FOUND", "The selected family was not found", 404).json()

        family_duplicate = Family.find_by_name(args["name"])
        if family_duplicate and family.id != family_duplicate.id:
            return Template4XX("FAMILY_DATA_DUPLICATION", "Another family with the same name already exist", 409).json()

        family.name = args["name"]
        family.save_to_db()
        return Template2XX("Name updated", 200, family.json()).json()


class FamilyDelete(Resource):
    """
        This is class is used to delete a family

        Methods:
        -------
        delete(self, _id)
            Delete a instrument family

        Error Code:
        ------------
        FAMILY_ID_NOT_FOUND
            The selected family was not found

        """

    @jwt_required_scope('metadata_del_family')
    def delete(self, family_id: int):
        family = Family.find_by_id(family_id)
        if not family:
            return Template4XX("FAMILY_ID_NOT_FOUND", "The selected family was not found", 404).json()
        family_json = family.json()
        family.delete_from_db()
        return Template2XX("Family deleted", 200, family_json).json()


class Instruments(Resource):
    """
    This is class is used to get all the instruments

    Methods:
    -------
    get(self)
        Give back a list of instruments

    Error Code:
    ------------
    INSTRUMENT_EMPTY_LIST
        No instruments found

    """

    @jwt_required_scope('metadata_get_instruments')
    def get(self):
        instruments = [ins.json() for ins in Instrument.get_all()]
        if not instruments:
            return Template4XX("INSTRUMENT_EMPTY_LIST", "No instruments found", 404).json()
        return Template2XX("Success", 200, {"instruments": instruments}).json()


class InstrumentSearch(SearchItemTemplate):
    """
        This is class is used to search instrument with a criteria

        Methods:
        -------
        post(self)
            Give back a list of instruments

        Error Code:
        ------------
        INSTRUMENT_EMPTY_LIST
            No instruments found

        INSTRUMENT_MALFORMED_REQUEST
            Invalid argument value

    """
    EMPTY_LIST = ("INSTRUMENT_EMPTY_LIST", "No instruments found")
    MALFORMED_REQUEST = ("INSTRUMENT_MALFORMED_REQUEST", "Invalid argument value")

    return_name = "instruments"
    function_search = Instrument
    api_endpoint = "metadata_search_instrument"


class InstrumentResource(Resource):
    """
        This is class is used to create and update instruments

        Methods:
        -------
        post(self)
            Create a new instrument

        put(self)
            Update an existing instrument

        Error Code:
        ------------
        INSTRUMENT_DATA_DUPLICATION
            The given instrument already exist

        INSTRUMENT_FAMILY_NOT_FOUND
            The instrument family was not found

        INSTRUMENT_ID_NOT_FOUND
            The selected instrument  was not found

    """
    post_parser = reqparse.RequestParser()
    post_parser.add_argument(
        'name',
        type=str,
        required=True,
        help="This field cannot be blank."
    )
    post_parser.add_argument(
        'familyId',
        type=str,
        required=True,
        help="This field cannot be blank."
    )

    put_parser = post_parser.copy()
    put_parser.add_argument(
        'id',
        type=int,
        required=True,
        help="This field cannot be blank."
    )

    @jwt_required_scope('metadata_add_instrument')
    def post(self):
        args = self.post_parser.parse_args()

        if not Family.find_by_id(args["familyId"]):
            return Template4XX("INSTRUMENT_FAMILY_NOT_FOUND", "The instrument family was not found", 404).json()

        if Instrument.find_by_instrument(args["name"], args["familyId"]):
            return Template4XX("INSTRUMENT_DATA_DUPLICATION", "The given instrument already exist", 409).json()

        instrument = Instrument(args["name"], args["familyId"])
        instrument.save_to_db()
        return Template2XX("Instrument created", 200, instrument.json()).json()

    @jwt_required_scope('metadata_update_instrument')
    def put(self):
        args = self.put_parser.parse_args()

        if not Family.find_by_id(args["familyId"]):
            return Template4XX("INSTRUMENT_FAMILY_NOT_FOUND", "The instrument family was not found", 404).json()

        if Instrument.find_by_instrument(args["name"], args["familyId"]):
            return Template4XX("INSTRUMENT_DATA_DUPLICATION", "The given instrument already exist", 409).json()

        instrument = Instrument.find_by_id(args["id"])
        if not instrument:
            return Template4XX("INSTRUMENT_ID_NOT_FOUND", "The selected instrument  was not found", 409).json()

        instrument.name = args["name"]
        instrument.family_id = args["familyId"]
        instrument.save_to_db()
        return Template2XX("Instrument updated", 200, instrument.json()).json()


class InstrumentDelete(Resource):
    """
        This is class is used to delete a instrument

        Methods:
        -------
        delete(self, _id)
            Delete a instrument

        Error Code:
        ------------
        INSTRUMENT_ID_NOT_FOUND
            The selected instrument was not found

        """

    @jwt_required_scope('metadata_del_instrument')
    def delete(self, instrument_id: int):
        instrument = Instrument.find_by_id(instrument_id)
        if not instrument:
            return Template4XX("INSTRUMENT_ID_NOT_FOUND", "The selected instrument was not found", 404).json()
        instrument_json = instrument.json()
        instrument.delete_from_db()
        return Template2XX("Instrument deleted", 200, instrument_json).json()
