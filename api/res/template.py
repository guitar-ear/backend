from flask_restful import Resource, reqparse
from sqlalchemy.exc import DataError

from api.jwt import jwt_required_scope
from api.res.response import Template2XX, Template4XX


class SearchItemTemplate(Resource):
    MALFORMED_REQUEST = ("'OBJECT'_EMPTY_LIST", "No 'object' found")
    EMPTY_LIST = ("'OBJECT'_EMPTY_LIST", "Invalid argument value")

    function_search = None
    return_name = "objects"
    api_endpoint = ""

    """
        This is class is a template for the the search endpoint

        Methods:
        -------
        post(self)
            Give back a list of 'objects'

        Error Code:
        ------------
        'OBJECT'_EMPTY_LIST
            No 'object' found

        'OBJECT'_MALFORMED_REQUEST
            Invalid argument value

    """

    post_parser = reqparse.RequestParser()
    post_parser.add_argument(
        'search',
        type=str,
        required=True,
        help="This field cannot be blank."
    )
    post_parser.add_argument(
        'offset',
        type=int,
        required=False,
        default=0
    )
    post_parser.add_argument(
        'delta',
        type=int,
        required=False,
        default=5
    )

    @jwt_required_scope(api_endpoint)
    def post(self):
        kwargs = self.post_parser.parse_args()
        try:
            objects = self.function_search.advanced_search(**kwargs)
        except DataError:
            return Template4XX(self.MALFORMED_REQUEST[0], self.MALFORMED_REQUEST[1], 400).json()
        if not objects:
            return Template4XX("INSTRUMENT_EMPTY_LIST", "Invalid argument value", 404).json()
        return Template2XX("Success", 200, {self.return_name: [obj.json() for obj in objects]}).json()
