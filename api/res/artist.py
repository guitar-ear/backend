import werkzeug
import io
from PIL import Image
from flask import send_file
from flask_restful import Resource, reqparse

from api.jwt import jwt_required_scope
from api.res.response import Template2XX, Template4XX, Template4XXPayload
from api.mod.artist import Artist
from api.mod.album import Album
from api.res.template import SearchItemTemplate


class Artists(Resource):
    """
    This is class is used to get all the artists

    Methods:
    -------
    get(self)
        Give back a list of artists

    Error Code:
    ------------
    ARTIST_EMPTY_LIST
        No artist found

    """

    @jwt_required_scope('metadata_get_artists')
    def get(self):
        artists = [artist.json() for artist in Artist.get_all()]
        if not artists:
            return Template4XX("ARTIST_EMPTY_LIST", "No artist found", 404).json()
        return Template2XX("Success", 200, {"artists": artists}).json()


class ArtistSearch(SearchItemTemplate):
    """
        This is class is used to search a artist with a criteria

        Methods:
        -------
        post(self)
            Give back a list of artist

        Error Code:
        ------------
        ARTIST_EMPTY_LIST
            No artist found

        ARTIST_MALFORMED_REQUEST
            Invalid argument value

    """
    EMPTY_LIST = ("ARTIST_EMPTY_LIST", "No artist found")
    MALFORMED_REQUEST = ("ARTIST_MALFORMED_REQUEST", "Invalid argument value")

    return_name = "artists"
    function_search = Artist
    api_endpoint = "metadata_search_artist"


class ArtistResource(Resource):
    """
        This is class is used to create and update a artist

        Methods:
        -------
        post(self)
            Create a new artist

        put(self)
            Update an existing artist

        Error Code:
        ------------
        ARTIST_DATA_DUPLICATION
            The given artist already exist

        ARTIST_ID_NOT_FOUND
            The selected artist was not found

    """
    post_parser = reqparse.RequestParser()
    post_parser.add_argument(
        'stageName',
        type=str,
        required=True,
        help="This field cannot be blank."
    )
    post_parser.add_argument(
        'name',
        type=str,
        required=False,
    )
    post_parser.add_argument(
        'surname',
        type=str,
        required=False
    )

    put_parser = post_parser.copy()
    put_parser.replace_argument(
        'stageName',
        type=str,
        required=False
    )
    put_parser.add_argument(
        'id',
        type=int,
        required=True,
        help="This field cannot be blank."
    )

    @jwt_required_scope('metadata_add_artist')
    def post(self):
        args = self.post_parser.parse_args()

        if Artist.find_by_stage_name(args["stageName"]):
            return Template4XX("ARTIST_DATA_DUPLICATION", "The given artist already exist", 409).json()

        artist = Artist(args["stageName"], args["name"], args["surname"])
        artist.save_to_db()
        return Template2XX("Artist created", 200, artist.json()).json()

    @jwt_required_scope('metadata_update_artist')
    def put(self):
        args = self.put_parser.parse_args()

        artist = Artist.find_by_id(args["id"])
        if not artist:
            return Template4XX("ARTIST_ID_NOT_FOUND", "The selected artist was not found", 404).json()

        artist_duplicate = Artist.find_by_stage_name(args["stageName"])
        if artist_duplicate and artist.id != artist_duplicate.id:
            return Template4XX("ARTIST_DATA_DUPLICATION", "The given artist already exist", 409).json()

        if args["stageName"] is not None:
            artist.stage_name = args["stageName"]
        if args["name"] is not None:
            artist.name = args["name"]
        if args["surname"] is not None:
            artist.surname = args["surname"]
        artist.save_to_db()

        return Template2XX("Artist updated", 200, artist.json()).json()


class ArtistDelete(Resource):
    """
        This is class is used to delete a artist

        Methods:
        -------
        delete(self, _id)
            Delete a artist

        Error Code:
        ------------
        ARTIST_ID_NOT_FOUND
            The selected artist was not found

    """

    @jwt_required_scope('metadata_del_artist')
    def delete(self, artist_id: int):
        artist = Artist.find_by_id(artist_id)
        if not artist:
            return Template4XX("ARTIST_ID_NOT_FOUND", "The selected artist was not found", 404).json()
        artist_json = artist.json()
        artist.delete_from_db()
        return Template2XX("Artist deleted", 200, artist_json).json()


class ArtistCreateAlbum(Resource):
    """
        This is class is used to add new album at the artist

        Methods:
        -------
        post(self)
            Add new album

        Error Code:
        ------------
        ARTIST_ID_NOT_FOUND
            The artist was not found

        ARTIST_ALBUM_ID_NOT_FOUND
            The selected albums ids were not found

    """

    put_parser = reqparse.RequestParser()
    put_parser.add_argument(
        'albumList',
        type=list,
        location='json',
        required=True,
        help="This field cannot be blank."
    )

    @jwt_required_scope('metadata_add_artist_album')
    def post(self, artist_id):
        artist = Artist.find_by_id(artist_id)
        if not artist:
            return Template4XX("ARTIST_ID_NOT_FOUND", "The selected artist was not found", 404).json()
        result, response_list = Artist.save_to_db_album(artist_id, self.put_parser.parse_args()["albumList"])
        if result:
            return Template2XX("Album added",
                               200,
                               {"albums": [album.json() for album in response_list]}
                               ).json()
        return Template4XXPayload("ARTIST_ALBUM_ID_NOT_FOUND",
                                  "The selected albums ids were not found",
                                  404,
                                  {"albums": response_list}
                                  ).json()


class ArtistDeleteAlbum(Resource):
    """
        This is class is used to delete a album from the artist

        Methods:
        -------
        delete(self, artist_id, album_id)
            Delete a album

        Error Code:
        ------------
        ARTIST_ID_NOT_FOUND
            The artist was not found

        ARTIST_ALBUM_ID_NOT_FOUND
            The selected album id was not found

    """

    @jwt_required_scope('metadata_del_artist_album')
    def delete(self, artist_id, album_id):
        artist = Artist.find_by_id(artist_id)
        if not artist:
            return Template4XX("ARTIST_ID_NOT_FOUND", "The selected artist was not found", 404).json()
        response = artist.delete_album(album_id)
        if response:
            return Template2XX("Album removed", 200, response.json()).json()
        return Template4XX("ARTIST_ALBUM_ID_NOT_FOUND", "The selected album id was not found", 404).json()


class ArtistCreateSong(Resource):
    """
        This is class is used to add new song at the artist

        Methods:
        -------
        post(self, artist_id)
            Add new song

        Error Code:
        ------------
        ARTIST_ID_NOT_FOUND
            The artist was not found

        ARTIST_SONG_ID_NOT_FOUND
            The selected songs ids were not found

    """

    put_parser = reqparse.RequestParser()
    put_parser.add_argument(
        'songList',
        type=list,
        location='json',
        required=True,
        help="This field cannot be blank."
    )

    @jwt_required_scope('metadata_add_artist_song')
    def post(self, artist_id):
        artist = Artist.find_by_id(artist_id)
        if not artist:
            return Template4XX("ARTIST_ID_NOT_FOUND", "The selected artist was not found", 404).json()
        result, response_list = Artist.save_to_db_song(artist_id, self.put_parser.parse_args()["songList"])
        if result:
            return Template2XX("Song added",
                               200,
                               {"songs": [album.json() for album in response_list]}
                               ).json()
        return Template4XXPayload("ARTIST_SONG_ID_NOT_FOUND",
                                  "The selected songs ids were not found",
                                  404,
                                  {"songs": response_list}
                                  ).json()


class ArtistDeleteSong(Resource):
    """
        This is class is used to delete a song from the artist

        Methods:
        -------
        delete(self, artist_id, song_id)
            Delete a song

        Error Code:
        ------------
        ARTIST_ID_NOT_FOUND
            The artist was not found

        ARTIST_SONG_ID_NOT_FOUND
            The selected song id was not found

    """

    @jwt_required_scope('metadata_del_artist_song')
    def delete(self, artist_id, song_id):
        artist = Artist.find_by_id(artist_id)
        if not artist:
            return Template4XX("ARTIST_ID_NOT_FOUND", "The selected artist was not found", 404).json()

        response = artist.delete_song(song_id)
        if response:
            return Template2XX("Song removed", 200, response.json()).json()
        return Template4XX("ARTIST_SONG_ID_NOT_FOUND", "The selected song id was not found", 404).json()


class ArtistImage(Resource):
    """
        This is class is used to inset and retrieve the artist image

        Methods:
        -------
        get(self, group_id)
            Retrieve image

        post(self, group_id)
            Add a image

        Error Code:
        ------------

        ARTIST_ID_NOT_FOUND
            The selected album was not found

        ARTIST_IMAGE_ERROR
            The given image is not valid

        ARTIST_MAX_IMAGE_SIZE
            The selected image is over 1 MB

        ARTIST_IMAGE_NOT_FOUND
            The selected image was not found

    """
    image_parser = reqparse.RequestParser()
    image_parser.add_argument(
        'image',
        type=werkzeug.datastructures.FileStorage,
        required=True,
        location='files'
    )

    @jwt_required_scope('metadata_get_artist_image')
    def get(self, artist_id):
        artist = Artist.find_by_id(artist_id)
        if not artist:
            return Template4XX("ARTIST_ID_NOT_FOUND", "The selected album was not found", 404).json()
        return send_file(io.BytesIO(artist.image),
                         attachment_filename='artist.jpeg',
                         mimetype='image/jpg'
                         )

    @jwt_required_scope('metadata_add_artist_image')
    def post(self, artist_id):
        artist = Artist.find_by_id(artist_id)
        if not artist:
            return Template4XX("ARTIST_ID_NOT_FOUND", "The selected album was not found", 404).json()
        args = self.image_parser.parse_args()
        if len(args["image"].read()) > 1048576:  # Size 1 MB
            return Template4XX("ARTIST_MAX_IMAGE_SIZE", "The selected image is over 1 MB", 400)
        try:
            im = Image.open(args["image"])
        except Exception:
            return Template4XX("ARTIST_IMAGE_ERROR", "The given image is not valid", 400).json()
        if im.format != "PNG" and im.format != "JPEG":
            return Template4XX("ARTIST_IMAGE_ERROR", "The given image is not valid", 400).json()
        if im.format == "PNG":
            im = im.convert('RGB')
        img_byte_arr = io.BytesIO()
        im.save(img_byte_arr, format='JPEG')
        artist.image = img_byte_arr.getvalue()
        artist.save_to_db()
        return Template2XX("Image Saved", 200).json()
