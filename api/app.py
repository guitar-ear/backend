import os
from flask import Flask

from .db import db
from .migrate import migrate
from .redis import redis_client
from .jwt import jwt
from .routes import api


def create_app(config=None):
    app = Flask(__name__)

    # load default configuration
    app.config.from_object('api.settings')

    # load environment configuration
    if 'WEBSITE_CONF' in os.environ:
        app.config.from_envvar('WEBSITE_CONF')

    # load app specified configuration
    if config is not None:
        if isinstance(config, dict):
            app.config.update(config)
        elif config.endswith('.py'):
            app.config.from_pyfile(config)

    setup_app(app)
    return app


def setup_app(app):
    # Create tables if they do not exist already
    @app.before_first_request
    def create_tables():
        db.create_all()

    # INIT DB
    db.init_app(app)
    migrate.init_app(app, db)

    # INIT REDIS
    redis_client.init_app(app)

    # INIT JWT
    jwt.init_app(app)

    # INIT ENDPOINT
    api.init_app(app)
