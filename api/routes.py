from flask_restful import Api

from .res.instrument import Families, FamilySearch, FamilyResource, FamilyDelete
from .res.instrument import Instruments, InstrumentSearch, InstrumentResource, InstrumentDelete
from .res.song import Songs, SongSearch, SongResource, SongDelete
from .res.album import Albums, AlbumSearch, AlbumResource, AlbumDelete, AlbumImage
from .res.artist import Artists, ArtistSearch, ArtistResource, ArtistDelete
from .res.artist import ArtistCreateAlbum, ArtistDeleteAlbum, ArtistCreateSong, ArtistDeleteSong, ArtistImage
from .res.group import Groups, GroupSearch, GroupResource, GroupDelete
from .res.group import GroupArtist, GroupAlbum, GroupSong, GroupImage

api = Api()

# Config
API_VERSION = '/v0'

# INSTRUMENT ENDPOINT
api.add_resource(Instruments, API_VERSION + '/instruments')
api.add_resource(InstrumentResource, API_VERSION + '/instrument')
api.add_resource(InstrumentSearch, API_VERSION + '/instrument/search')
api.add_resource(InstrumentDelete, API_VERSION + '/instrument/<int:instrument_id>')

# FAMILY INSTRUMENT ENDPOINT
api.add_resource(Families, API_VERSION + '/instrument/families')
api.add_resource(FamilyResource, API_VERSION + '/instrument/family')
api.add_resource(FamilySearch, API_VERSION + '/instrument/family/search')
api.add_resource(FamilyDelete, API_VERSION + '/instrument/family/<int:family_id>')

# SONG ENDPOINT
api.add_resource(Songs, API_VERSION + '/songs')
api.add_resource(SongResource, API_VERSION + '/song')
api.add_resource(SongSearch, API_VERSION + '/song/search')
api.add_resource(SongDelete, API_VERSION + '/song/<int:song_id>')

# ALBUM ENDPOINT
api.add_resource(Albums, API_VERSION + '/albums')
api.add_resource(AlbumResource, API_VERSION + '/album')
api.add_resource(AlbumSearch, API_VERSION + '/album/search')
api.add_resource(AlbumDelete, API_VERSION + '/album/<int:album_id>')
api.add_resource(AlbumImage, API_VERSION + '/album/<int:album_id>/image-upload')

# ARTIST ENDPOINT
api.add_resource(Artists, API_VERSION + '/artists')
api.add_resource(ArtistResource, API_VERSION + '/artist')
api.add_resource(ArtistSearch, API_VERSION + '/artist/search')
api.add_resource(ArtistDelete, API_VERSION + '/artist/<int:artist_id>')
api.add_resource(ArtistCreateAlbum, API_VERSION + '/artist/<int:artist_id>/album')
api.add_resource(ArtistDeleteAlbum, API_VERSION + '/artist/<int:artist_id>/album/<int:album_id>')
api.add_resource(ArtistCreateSong, API_VERSION + '/artist/<int:artist_id>/song')
api.add_resource(ArtistDeleteSong, API_VERSION + '/artist/<int:artist_id>/song/<int:song_id>')
api.add_resource(ArtistImage, API_VERSION + '/artist/<int:artist_id>/image-upload')

# GROUP ENDPOINT
api.add_resource(Groups, API_VERSION + '/groups')
api.add_resource(GroupResource, API_VERSION + '/group')
api.add_resource(GroupSearch, API_VERSION + '/group/search')
api.add_resource(GroupDelete, API_VERSION + '/group/<int:group_id>')
api.add_resource(GroupArtist, API_VERSION + '/group/<int:group_id>/artist/<int:artist_id>')
api.add_resource(GroupAlbum, API_VERSION + '/group/<int:group_id>/album/<int:album_id>')
api.add_resource(GroupSong, API_VERSION + '/group/<int:group_id>/song/<int:song_id>')
api.add_resource(GroupImage, API_VERSION + '/group/<int:group_id>/image-upload')
