from api.db import db


class Song(db.Model):
    __tablename__ = 'song'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), unique=True, nullable=False)

    def __init__(self, title: str):
        self.title = title

    def json(self) -> dict:
        return {
            'id': self.id,
            'title': self.title,
        }

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_title(cls, title: str):
        return cls.query.filter_by(title=title).first()

    @classmethod
    def find_by_id(cls, _id: int):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def advanced_search(cls, search: str, delta: int, offset: int = 0):
        search = "{}%".format(search)
        return cls.query.filter(Song.title.like(search)).offset(offset).limit(delta).all()
