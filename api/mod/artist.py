from api.db import db
from .song import Song
from .album import Album

artists_albums = db.Table('artists_albums',
                          db.Column('fk_artist', db.Integer, db.ForeignKey('artist.id')),
                          db.Column('fk_album', db.Integer, db.ForeignKey('album.id'))
                          )

artists_songs = db.Table('artists_songs',
                         db.Column('fk_artist', db.Integer, db.ForeignKey('artist.id')),
                         db.Column('fk_song', db.Integer, db.ForeignKey('song.id'))
                         )


class Artist(db.Model):
    __tablename__ = 'artist'

    id = db.Column(db.Integer, primary_key=True)
    stage_name = db.Column(db.String(255), unique=True, nullable=False)
    name = db.Column(db.String(255))
    image = db.Column(db.LargeBinary)
    surname = db.Column(db.String(255))
    albums = db.relationship("Album", secondary=artists_albums)
    songs = db.relationship("Song", secondary=artists_songs)

    def __init__(self, stage_name: str, name: str, surname: str, ):
        self.stage_name = stage_name
        self.name = name if name is not None else ""
        self.surname = surname if surname is not None else ""

    def json(self) -> dict:
        return {
            'id': self.id,
            'stageName': self.stage_name,
            'name': self.name,
            'surname': self.surname,
            'albumList': [album.id for album in self.albums],
            'songList': [song.json() for song in self.songs]
        }

    # NEED IMPROVEMENTS
    @staticmethod
    def save_to_db_album(_id, items):
        artist = Artist.find_by_id(_id)
        return artist.save_with_transaction(items, artist.albums, Album.find_by_id)

    @staticmethod
    def save_to_db_song(_id, items):
        artist = Artist.find_by_id(_id)
        return artist.save_with_transaction(items, artist.songs, Song.find_by_id)

    def save_with_transaction(self, items, selected_list, func):
        item_list = []
        error_list = []
        for item_id in items:
            item = func(item_id)
            if item:
                item_list.append(item)
            else:
                error_list.append(item_id)
        if len(error_list) > 0:
            return False, error_list
        selected_list.extend(item_list)
        self.save_to_db()
        return True, item_list

    # END NEED IMPROVEMENTS

    def delete_album(self, _id):
        return self._delete_song_album(_id, self.albums)

    def delete_song(self, _id):
        return self._delete_song_album(_id, self.songs)

    def _delete_song_album(self, _id, iterable):
        item = list(filter(lambda x: x.id == _id, iterable))
        if len(item) == 0:
            return None
        item = item[0]
        iterable.remove(item)
        self.save_to_db()
        return item

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_stage_name(cls, stage_name: str):
        return cls.query.filter_by(stage_name=stage_name).first()

    @classmethod
    def find_by_id(cls, _id: int):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def advanced_search(cls, search: str, delta: int, offset: int = 0):
        search = "{}%".format(search)
        return cls.query.filter(Artist.stage_name.like(search)).offset(offset).limit(delta).all()
