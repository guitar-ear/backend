from api.db import db
from .song import Song

albums_songs = db.Table('albums_songs',
                        db.Column('fk_album', db.Integer, db.ForeignKey('album.id')),
                        db.Column('fk_song', db.Integer, db.ForeignKey('song.id'))
                        )


class Album(db.Model):
    __tablename__ = 'album'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), unique=True, nullable=False)
    image = db.Column(db.LargeBinary)
    songs = db.relationship("Song", secondary=albums_songs)

    def __init__(self, title: str, song_list: list):
        self.title = title
        self.song_list = song_list

    def json(self) -> dict:
        return {
            'id': self.id,
            'title': self.title,
            'songList': [song.id for song in self.songs],
        }

    def save_with_transaction(self):
        item_list = []
        error_list = []
        for song_id in self.song_list:
            song = Song.find_by_id(song_id)
            if song:
                item_list.append(song)
            else:
                error_list.append(song_id)
        if len(error_list) > 0:
            return False, error_list
        self.songs.extend(item_list)
        self.save_to_db()
        return True, None

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_title(cls, title: str):
        return cls.query.filter_by(title=title).first()

    @classmethod
    def find_by_id(cls, _id: int):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def advanced_search(cls, search: str, delta: int, offset: int = 0):
        search = "{}%".format(search)
        return cls.query.filter(Album.name.like(search)).offset(offset).limit(delta).all()
