from api.db import db


class Family(db.Model):
    __tablename__ = 'family'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)

    def __init__(self, name: str):
        self.name = name

    def json(self) -> dict:
        return {
            'id': self.id,
            'name': self.name,
        }

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_name(cls, name: str):
        return cls.query.filter_by(name=name).first()

    @classmethod
    def find_by_id(cls, _id: int):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def advanced_search(cls, search: str, delta: int, offset: int = 0):
        search = "{}%".format(search)
        return cls.query.filter(Family.name.like(search)).offset(offset).limit(delta).all()


class Instrument(db.Model):
    __tablename__ = 'instrument'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    family_id = db.Column(db.Integer, db.ForeignKey('family.id'))
    family = db.relationship('Family')

    def __init__(self, name: str, family_id: int):
        self.name = name
        self.family_id = family_id

    def json(self) -> dict:
        return {
            'id': self.id,
            'name': self.name,
            'familyId': self.family_id,
        }

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_instrument(cls, name: str, family_id: int):
        return cls.query.filter_by(name=name, family_id=family_id).first()

    @classmethod
    def find_by_id(cls, _id: int):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def advanced_search(cls, search: str, delta: int, offset: int = 0):
        search = "{}%".format(search)
        return cls.query.filter(Instrument.name.like(search)).offset(offset).limit(delta).all()
