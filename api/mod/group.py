from api.db import db
from api.mod.artist import Artist
from api.mod.album import Album
from api.mod.song import Song

groups_artists = db.Table('groups_artists',
                          db.Column('fk_group', db.Integer, db.ForeignKey('group.id')),
                          db.Column('fk_artist', db.Integer, db.ForeignKey('artist.id'))
                          )

groups_albums = db.Table('groups_albums',
                         db.Column('fk_group', db.Integer, db.ForeignKey('group.id')),
                         db.Column('fk_album', db.Integer, db.ForeignKey('album.id'))
                         )

groups_songs = db.Table('groups_songs',
                        db.Column('fk_group', db.Integer, db.ForeignKey('group.id')),
                        db.Column('fk_song', db.Integer, db.ForeignKey('song.id'))
                        )


class Group(db.Model):
    __tablename__ = 'group'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)
    image = db.Column(db.LargeBinary)
    artists = db.relationship("Artist", secondary=groups_artists)
    albums = db.relationship("Album", secondary=groups_albums)
    songs = db.relationship("Song", secondary=groups_songs)

    def __init__(self, name: str):
        self.name = name

    def json(self) -> dict:
        return {
            'id': self.id,
            'name': self.name,
            'artistList': [artist.id for artist in self.artists],
            'albumList': [album.id for album in self.albums],
            'songList': [song.id for song in self.songs],
        }

    def insert_artist(self, _id_list: list):
        self._insert_transaction(_id_list, Artist.find_by_id, self.artists)

    def insert_album(self, _id_list: list):
        self._insert_transaction(_id_list, Album.find_by_id, self.albums)

    def insert_song(self, _id_list: list):
        self._insert_transaction(_id_list, Song.find_by_id, self.songs)

    def _insert_transaction(self, _id_list: list, mapper, iterable: list):
        original_list = set(iterable)
        item_list = set(map(lambda item: mapper(item), _id_list))
        iterable += list(item_list - original_list)
        self.save_to_db()

    def delete_artist(self, _id_list: list):
        self.artists = self._remove_transaction(_id_list, Artist.find_by_id, self.artists)
        self.save_to_db()

    def delete_album(self, _id_list: list):
        self.albums = self._remove_transaction(_id_list, Album.find_by_id, self.albums)
        self.save_to_db()

    def delete_song(self, _id_list: list):
        self.songs = self._remove_transaction(_id_list, Song.find_by_id, self.songs)
        self.save_to_db()

    @staticmethod
    def _remove_transaction(_id_list: list, mapper, iterable: list):
        original_list = set(iterable)
        item_list = set(map(lambda item: mapper(item), _id_list))
        return list(original_list - item_list)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        db.session.commit()

    @classmethod
    def get_all(cls):
        return cls.query.all()

    @classmethod
    def find_by_name(cls, name: str):
        return cls.query.filter_by(name=name).first()

    @classmethod
    def find_by_id(cls, _id: int):
        return cls.query.filter_by(id=_id).first()

    @classmethod
    def advanced_search(cls, search: str, delta: int, offset: int = 0):
        search = "{}%".format(search)
        return cls.query.filter(Artist.name.like(search)).offset(offset).limit(delta).all()
