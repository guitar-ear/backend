# Backend Final Project

Requirements:
1. Docker
2. Docker Compose
3. Python


## Installation Process
1. Docker

    You can install docker from [here.](https://docs.docker.com/install/ "Docker Installation Page") 

2. Docker Compose

    You can install docker compose from [here.](https://docs.docker.com/compose/install/ "Docker Installation Page")
    
3. Python

    You can install python from [here.](https://www.python.org/downloads/ "Python Download Page") 
    
## Setup

...

## Endpoint Table

:white_check_mark: : Implemented

:red_circle: : Not implemented

:x: : Deprecated

| Created    | Edited     | Version | Path                               | Server             | Client       |
| :---       | :---       | :------:| :---                               | :----------------: | :----------: |
| 2019-02-28 | 2019-02-28 | V0.1    | /api/v1/user/register              | :white_check_mark: | :red_circle: |
| 2019-02-28 | 2019-02-28 | V0.1    | /api/v1/user/login                 | :white_check_mark: | :red_circle: |
| 2019-02-28 | 2019-02-28 | V0.1    | /api/v1/user/refresh               | :white_check_mark: | :red_circle: |
| 2019-02-28 | 2019-02-28 | V0.1    | /api/v1/user/logout                | :white_check_mark: | :red_circle: |
| 2019-02-28 | 2019-02-28 | V0.1    | /api/v1/user/delete/{int:user_id}  | :white_check_mark: | :red_circle: |
 
