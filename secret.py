import os

# SECRET KEYS
SECRET_KEY = os.environ.get('SECRET_KEY', "secret")


# DB CONFIG
PG_HOST = os.environ.get('PG_HOST', None)  # 127.0.0.1
PG_PORT = os.environ.get('PG_PORT', None)  # 5432
PG_USER = os.environ.get('PG_USER', None)  # postgres
PG_PW = os.environ.get('PG_PW', None)  # password
PG_DB = os.environ.get('PG_DB', None)  # guitar_ear
POSTGRES_URL = f'postgresql+psycopg2://{PG_USER}:{PG_PW}@{PG_HOST}:{PG_PORT}/{PG_DB}'

# REDIS CONFIG
REDIS_URI = os.environ.get('REDIS_URI', None)  # localhost:6379
REDIS_PW = os.environ.get('REDIS_PW', None)
REDIS_URL = f'redis://:{REDIS_PW}@{REDIS_URI}/0'
