#!/bin/sh

echo "Waiting for postgres..."

while ! nc -z "$PG_HOST" "$PG_PORT"; do
  sleep 0.1
done

echo "PostgreSQL started"

exec "$@"
