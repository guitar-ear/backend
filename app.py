from api.app import create_app
from secret import SECRET_KEY, POSTGRES_URL, REDIS_URL

app = create_app({
    # FLASK CONFIG
    'SECRET_KEY': SECRET_KEY,

    # DB CONFIG
    'SQLALCHEMY_TRACK_MODIFICATIONS': False,
    'SQLALCHEMY_DATABASE_URI': POSTGRES_URL,

    # REDIS CONFIG
    'REDIS_URL': REDIS_URL,

    # JWT CONFIG
    'JWT_ALGORITHM': 'HS512'
})
